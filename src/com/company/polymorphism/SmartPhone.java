package com.company.polymorphism;

public class SmartPhone extends MobilePhone{

    private String name;

    public SmartPhone() {
        super(300000.00);
        setName("Apple");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void makeCall(){
        System.out.println("Make a call using " + name + " phone");
    }


}
