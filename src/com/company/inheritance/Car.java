package com.company.inheritance;

public class Car extends Vehicle{

    protected String type = "Car";

    public void drive(){

        //invoking the super class drive method
        super.drive();
        System.out.println("This is a car class drive method");
    }

}
