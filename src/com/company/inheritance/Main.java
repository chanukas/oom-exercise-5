package com.company.inheritance;

public class Main {

    public static void main(String[] args) {
        Tesla tesla = new Tesla();

        System.out.println(tesla.brand +" is a "+ tesla.type);
        tesla.drive();

        Bmw bmw = new Bmw();

        System.out.println(bmw.brand +" is a "+ bmw.type);
        bmw.drive();

    }
}
