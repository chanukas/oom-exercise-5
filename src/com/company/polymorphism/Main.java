package com.company.polymorphism;

public class Main {

    public static void main(String[] args) {

        MobilePhone mobilePhone = new MobilePhone(5000);

        System.out.println("price :" + mobilePhone.getPrice());

        mobilePhone.charge();
        mobilePhone.makeCall();

        // this is the run time polymorphism
        mobilePhone = new SmartPhone();

        System.out.println("price :" + mobilePhone.getPrice());

        mobilePhone.charge();
        mobilePhone.makeCall();

    }
}
