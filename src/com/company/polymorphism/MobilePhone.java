package com.company.polymorphism;

public class MobilePhone {
    private double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public MobilePhone(double price){
        this.price = price;
    }

    public void makeCall(){
        System.out.println("Calling...");
    }

    public void charge(){
        System.out.println("Charging...");
    }


}
